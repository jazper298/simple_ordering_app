import 'package:flutter/material.dart';

class CartItem{
  String id;
  String name;
  String price;

  CartItem(this.id, this.name, this.price);

  //Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    map['price'] = price;

    return map;
  }

  //Extract a Note object from a Map object
  CartItem.fromMapObject(Map<String, dynamic> map) {
    this.id = map['id'];
    this.name = map['name'];
    this.price = map['price'];
  }
}