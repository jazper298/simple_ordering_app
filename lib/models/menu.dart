class Menu {
  String menuid;
  String menuname;
  String menudescription;
  String menuprice;
  String menuimg;

  Menu(this.menuid, this.menuname, this.menudescription, this.menuprice,
      this.menuimg);

  //Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['menuid'] = menuid;
    map['menuname'] = menuname;
    map['menudescription'] = menudescription;
    map['menuprice'] = menuprice;
    map['menuimg'] = menuimg;

    return map;
  }

  //Extract a Note object from a Map object
  Menu.fromMapObject(Map<String, dynamic> map) {
    this.menuid = map['menuid'];
    this.menuname = map['menuname'];
    this.menudescription = map['menudescription'];
    this.menuprice = map['menuprice'];
    this.menuimg = map['menuimg'];
  }
}
