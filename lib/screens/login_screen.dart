import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_ordering_app/screens/main_page.dart';
import 'package:simple_ordering_app/widgets/widgets.dart';

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isLoading = false;
  final formKey = GlobalKey<FormState>();
  TextEditingController emailTextEditingController = new TextEditingController();
  TextEditingController passwordTextEditingController = new TextEditingController();

  signIn(String email, pass) async{
    setState(() {
      _isLoading = true;
    });

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map data = {
      'username': email,
      'password': pass
    };
    print(email);
    print(pass);
    var jsonResponse;
    var response = await http.post(
        "https://bigbys.e2econsultancy.ph/api/apitest/login.php",
        body: data);
   // var message = jsonDecode(response.body.toString());
    print(response.statusCode);
    print(response.body);

    // If the Response Message is Matched.
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body.toString());
      print(jsonResponse);
      if (jsonResponse != null && !response.body.contains("Failed")) {
        setState(() {
          _isLoading = false;
        });
        // Navigate to Profile Screen & Sending Email to Next Screen.
        sharedPreferences.setString('userid', jsonResponse['userid']);
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MainPage())
        );
      }
      else {
        // If Email or Password did not Matched.
        // Hiding the CircularProgressIndicator.
        setState(() {
          _isLoading = false;
        });
    //
    //     // Showing Alert Dialog with Response JSON Message.
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("Unauthorized. Please check Username and Password."),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent));
    return Scaffold(
      body: Container(
        child: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
          children: <Widget>[
            SizedBox(
              height: 50.0,
            ),
            headerSection(),
            SizedBox(
              height: 50.0,
            ),
            textSection(),
            SizedBox(
              height: 50.0,
            ),
            buttonSection(),
          ],
        ),
      ),
    );
  }
  Container headerSection() {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 50.0),
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
      child: Text("Login",
          style: TextStyle(
              color: Colors.black,
              fontSize: 35.0,
              fontWeight: FontWeight.bold)),
    );
  }
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  Container textSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 20.0),
      child: Column(
        children: <Widget>[
          TextFormField(
            controller: emailController,
            cursorColor: Colors.black,

            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
              hintText: "Username",
              border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
              hintStyle: TextStyle(color: Colors.black),
            ),
          ),
          SizedBox(height: 30.0),
          TextFormField(
            controller: passwordController,
            cursorColor: Colors.black,
            obscureText: true,
            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
              hintText: "Password",
              border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
              hintStyle: TextStyle(color: Colors.black),
            ),
          ),
        ],
      ),
    );
  }


  Container buttonSection() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      padding: EdgeInsets.symmetric(horizontal: 50.0),
      margin: EdgeInsets.only(top: 15.0),
      child: RaisedButton(
        color: Colors.red,
        onPressed: emailController.text == "" || passwordController.text == "" ? null : () {
          setState(() {
            _isLoading = true;
          });
          signIn(emailController.text.trim(), passwordController.text.trim());
        },
        elevation: 0.0,
        child: Text("Login", style: TextStyle(color: Colors.white70)),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
        ),
      ),
    );
  }
}
